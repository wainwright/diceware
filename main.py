#!/usr/bin/env python3

import math
import secrets
import argparse
from tabulate import tabulate

def load_words(file):
	words = set()
	for l in file:
		words.add(l.split()[-1])
	return words

def get_entropy(options):
	'''Returns entropy per choice '''
	return math.log2(options)

def make_phrase(words, number):
	passphrase = []
	for i in range(number):
		word = secrets.choice(list(words))
		passphrase.append(word)
	return passphrase

def make_short_words(wordlist, short_length):
	short_words = set()
	for w in wordlist:
		short_words.add(w[:short_length])
	return short_words

def find_full_entropy(wordlist):
	return get_entropy(len(wordlist))

def find_short_entropy(wordlist, short_length):
	short_words = make_short_words(wordlist, short_length)
	return get_entropy(len(short_words))

def check_short_entropy(wordlist, short_length):
	short_words = make_short_words(wordlist, short_length)
	assert len(wordlist) >= len(short_words)
	if len(short_words) < len(wordlist):
		print(
				f'\n'
				f'!WARNING: not all words in shortlist are unique.  '
				f'The resulting wordlist has {get_entropy(len(short_words)):.1f} '
				f'bits of entropy per word, compared to '
				f'{get_entropy(len(wordlist)):.1f} bits per word in the full list'
				f'\n'
				)
	return len(short_words)

def passphrase_entropy(wordlist, num_words, short_length):
	len_short_words = check_short_entropy(wordlist, short_length)
	entropy = num_words*get_entropy(min(len_short_words, len(wordlist)))
	return entropy

def parse_args():
		parser = argparse.ArgumentParser(description='Passphrase generator')
		parser.add_argument('wordlist', type=argparse.FileType('r'),
				help='word list file')
		parser.add_argument('-s', '--shorten', help='shorten words to length',
				type=int)
		parser.add_argument('-j', '--join', help='join character',
				default='', type=str)
		group = parser.add_mutually_exclusive_group()
		group.add_argument('-n', '--number', help='number of words', type=int)
		group.add_argument('-e', '--minimum-entropy', help='generate a password'
				'with at least this much entropy', type=int)
		return parser.parse_args()

def run(word_list, number, short, join, minimum_entropy):
	'''Run wordlist programme'''
	if all([number, minimum_entropy]):
		print('Please specify only one of number of words or minimum entropy')
		return -1
	if all(not a for a in [number, minimum_entropy]):
		print('Please specify one of number of words or minimum entropy')
		return -1

	short = int(short) if short else None
	minimum_entropy = int(minimum_entropy) if minimum_entropy else None
	words = load_words(word_list)

	entropy_per_word = find_short_entropy(words, short) \
			if short \
			else find_full_entropy(words)

	if minimum_entropy:
		number = math.ceil(minimum_entropy / entropy_per_word)

	entropy = passphrase_entropy(words, number, short)

	passphrase = make_phrase(words, number)
	short_phrase = []

	result = [range(number), passphrase]
	if short:
		for w in passphrase:
			short_phrase.append(w[:short])
		result.append(short_phrase)
	print(tabulate(result))

	print(f'Passphrase ({entropy:.1f} bits of entropy):')
	finished_phrase = join.join(short_phrase if short else passphrase)
	print(finished_phrase)

if __name__ == '__main__':
	args = parse_args()
	run(
			word_list=args.wordlist,
			number=args.number,
			short=args.shorten,
			join=args.join,
			minimum_entropy=args.minimum_entropy
			)
